use Shops

select * from Shop
select * from Owner
select * from Ownership

exec YoungestEntrepreneurInRegion 'Київський'
select * from OwnersLess18Age


create login LogManager with password = '1234'
create user Manager for login LogManager

create role ManagerRole AUTHORIZATION Manager
alter role ManagerRole add MEMBER Manager

grant all on [dbo].[Shop] to ManagerRole
grant all on [dbo].[Ownership] to ManagerRole
grant all on [dbo].[Owner] to ManagerRole
grant select on [dbo].[OwnersLess18Age] to ManagerRole
grant execute on [dbo].[YoungestEntrepreneurInRegion] to ManagerRole
  
alter view OwnersLess18Age as
	select Ownership.id as RegistrationNumber, Ownership.date,
		Shop.name, Shop.region,
		Owner.full_name, Owner.birthday,
		datediff(year, Owner.birthday, Ownership.date) as Age
	from Owner
		inner join Ownership on Owner.id = Ownership.shop_id
		inner join Shop on Ownership.shop_id = Shop.id
	where datediff(year, Owner.birthday, Ownership.date) <= 20;


exec sp_addlogin @loginame = 'LogOwner', @passwd='1234';
exec sp_adduser 'LogOwner', 'Owner';

exec sp_addrole 'OwnerRole', 'Owner';
exec sp_addrolemember 'OwnerRole', 'Owner';

grant select on [dbo].[Shop] to OwnerRole
grant select on [dbo].[Ownership] to OwnerRole
grant select on [dbo].[Owner] to OwnerRole

update Owner set birthday = '2022-01-01' where id = 1;

insert into Shop(name, region, profile, capital)
values ('Сільпо', 'Київський', 'продукти', 10000.0);

select * from OwnersLess18Age

-- Всі користувачі
exec sp_helpuser


-- backups
BACKUP DATABASE Shops
	TO DISK = 'D:\db\lab_03\backups\backup.bak'
	WITH INIT, NOFORMAT,
	NAME = 'Shops Full DB backup',
	DESCRIPTION = 'Shops Full DB backup';

RESTORE DATABASE Shops
	FROM DISK = 'D:\db\lab_03\backups\backup.bak'
	WITH RECOVERY, REPLACE;


-- full
BACKUP DATABASE Shops
	TO DISK = 'D:\db\lab_03\backups\full.bak'
	WITH INIT, NOFORMAT,
	NAME = 'Shops Full DB backup',
	DESCRIPTION = 'Shops Full DB backup';

BACKUP LOG Shops
	TO DISK = 'D:\db\lab_03\backups\log.bak'
	WITH INIT, NOFORMAT, NORECOVERY,
	NAME = 'Shops Translog backup',
	DESCRIPTION = 'Shops Transaction Log backup';

RESTORE DATABASE Shops
	FROM DISK = 'D:\db\lab_03\backups\full.bak'
	WITH NORECOVERY, REPLACE;

RESTORE LOG Shops
	FROM DISK = 'D:\db\lab_03\backups\log.bak'
	WITH NORECOVERY;


-- differential
BACKUP DATABASE Shops
	TO DISK = 'D:\db\lab_03\backups\diff.bak'
	WITH INIT, NOFORMAT, DIFFERENTIAL,
	NAME = 'Shops Diff DB backup',
	DESCRIPTION = 'Shops Differential DB backup';

RESTORE DATABASE Shops
	FROM DISK = 'D:\db\lab_03\backups\full.bak'
	WITH NORECOVERY, REPLACE;

RESTORE DATABASE Shops
	FROM DISK = 'D:\db\lab_03\backups\diff.bak'
	WITH NORECOVERY;